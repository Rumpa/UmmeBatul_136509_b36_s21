<?php
namespace App\Modal;
use PDO;
use PDOException;
class Database{
    public $DBH;
    public $host="localhost";
    public $dbname="atomic_project_b36";
    public $username="root";
    public $password="";


    public function __construct(){
        try {
            $this->DBH = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->username, $this->password);
            echo "Connection Successful<br>";
        }
        catch(PDOException $error){
            echo $error->getMessage();
        }
    }

}
?>

